<?php

/**
 * Implements hook_menu().
 **/
function list_value_alter_menu(){
  return array(
    'admin/config/development/list-value-alter' => array(
      'title' => 'Change the list field value',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('list_value_alter_form'),
      'access arguments' => array('administer contents'),
    ),
  );
}

function list_value_alter_form($form, &$form_state) {
  $listFields = _all_list_fields('name');
  $default = !empty($form_state['values']['field_name']) ? $form_state['values']['field_name'] : current($listFields['fields']);
  $form['field_name'] = array(
    '#type' => 'select',
    '#title' => t('List fields(text)'),
    '#description' => t('Select the list field'),
    '#default_value' => $default,
    '#options' => $listFields['fields'],
    '#ajax' => array(
      'callback' => '_list_allowed_values',
      'wrapper' => 'list-values',
      'method' => 'replace',
    ),
  );
  foreach ($listFields['type'] as $key => $value) {
    $form[$key] = array(
      '#type' => 'hidden',
      '#value' => $value
    );
  }
  $header = array('Existing value', 'New value');
  $options = array('0' => array('test1', 'test2'));
  $form['mapping'] = array(
    '#prefix' => '<div id="list-values">',
    '#suffix' => '</div>',
    '#theme' => 'list_value_alter_mapping_table',
    '#tree' => TRUE,
  );
	$allListFields = _all_list_fields('allowed_values');
  foreach ($allListFields[$default] as $key => $field_name) {
    $form['mapping'][] = array(
      '#type' => 'textfield',
      '#value' => $key . '|' . $field_name,
      '#extra_data' => array('field_name' => $key . '|' . $field_name)
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save mapping'),
  );
  return $form;
}

function list_value_alter_form_validate($form, &$form_state) {
  $fieldName = $form_state['input']['field_name'];
  $fieldType = $form_state['input'][$fieldName];
  $mappings = $form_state['input']['mapping'];
  $values = array();
  $count = 0;
  foreach($mappings as $key => $option){
    $value = list_extract_allowed_values($option, $fieldType, FALSE);
    $key = key($value);
    $element = 'mapping['.$count++.']';
    if ($fieldType == 'list_integer' && !preg_match('/^-?\d+$/', $key)) {
      form_set_error($element, t('Allowed values list: keys must be integers.'));
      break;
    }
    if ($fieldType == 'list_float' && !is_numeric($key)) {
      form_set_error($element, t('Allowed values list: each key must be a valid integer or decimal.'));
      break;
    }
    elseif ($fieldType == 'list_text' && drupal_strlen($key) > 255) {
      form_set_error($element, t('Allowed values list: each key must be a string at most 255 characters long.'));
      break;
    }
  }
}

function list_value_alter_form_submit($form, &$form_state) {
  $list_field = $form_state['values']['field_name'];
  $raw_orig_value = $form_state['values']['mapping'];
  $raw_updated_value = $form_state['input']['mapping'];
  $orig_value = _list_actual_values($raw_orig_value);
  $updated_value = _list_actual_values($raw_updated_value);
  $raw_diff_map = array_diff($raw_updated_value, $raw_orig_value);
  $diff_map_key = array_diff($orig_value['keys'], $updated_value['keys']);
  $diff_map_value = array_diff($orig_value['values'], $updated_value['values']);
  if (!empty($diff_map_key)) {
    foreach ($diff_map_key as $key => $value) {
      $entities = db_select('field_data_' . $list_field, 'fd')
        ->fields('fd', array('entity_id', 'entity_type'))
        ->condition($list_field . '_value', $value, '=')
        ->execute()
        ->fetchAll();
      $new_key = $updated_value['keys'][$key];
      foreach ($entities as $entity) {
        $raw_entity = entity_load($entity->entity_type, array($entity->entity_id));
        $entity_wrapper = entity_metadata_wrapper($entity->entity_type, $raw_entity[$entity->entity_id]);
        $entity_wrapper->$list_field->set($new_key);
        $entity_wrapper->save();
      }
      $info = field_info_field($list_field);
      $values = &$info['settings']['allowed_values'];
      $values[$new_key] = $updated_value['values'][$key];
      unset($values[$value]);
      field_update_field($info);
    }
  }
  elseif (!empty($diff_map_value)) {
    foreach ($diff_map_value as $key => $value) {
      $act_key = $updated_value['keys'][$key];
      $info = field_info_field($list_field);
      $values = &$info['settings']['allowed_values'];
      $values[$act_key] = $updated_value['values'][$key];
      field_update_field($info);
    }
  }
}

function _list_actual_values($list_val) {
  $actual_list = array();
  foreach ($list_val as $key => $value) {
    $lval = explode("|", $value);
    $actual_list['values'][] = $lval[1];
    $actual_list['keys'][] = $lval[0];
  }
  return $actual_list;
}

function _all_list_fields($property = NULL) {
  $fields = $fieldType = field_info_fields();
  array_walk($fields, 'list_fields_map', $property);
  if($property == 'name'){
    array_walk($fieldType, 'list_fields_map', 'field_type');
    return array('fields' => array_filter($fields), 'type' => array_filter($fieldType));
  }
  return array_filter($fields);
}

function list_fields_map(&$value, &$key, $property){
  $fieldTypes = array('list_float', 'list_integer', 'list_text');
  if(in_array($value['type'], $fieldTypes)){
    switch($property){
      case 'name':
        $value = $value['field_name'];
      break;
      case 'allowed_values':
        $value = $value['settings']['allowed_values'];
      break;
      case 'bundles':
        $value = $value['bundles'];
      break;
      case 'field_type':
        $value = $value['type'];
      break;
      default:
        $value = NULL;
      break;
    }
  }else{
    $value = NULL;
  }
}

function _list_allowed_values($form, $form_state) {
  return $form['mapping'];
}

function list_value_alter_theme() {
  return array(
    'list_value_alter_mapping_table' => array(
      'render element' => 'element'
    )
  );
}

function theme_list_value_alter_mapping_table($vars) {
  $element = $vars['element'];
  $rows = array();
  foreach (element_children($element) as $key) {
    $rows[] = array(
      array('data' => $element[$key]['#extra_data']['field_name']),
      array('data' => render($element[$key])),
      array('data' => l('delete', NULL,  array('fragment' => 'id'))),
    );
  }
  $title = t('<h3>List value mapping</h3>');
  $header = array(t('Existing value'), t('New value'), t('Opertations'));
  return $title . theme('table', array('header' => $header, 'rows' => $rows));
}
